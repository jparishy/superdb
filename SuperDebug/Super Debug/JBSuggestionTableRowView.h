//
//  JBSuggestionTableRowView.h
//  Super Debug
//
//  Created by Jason Brennan on 2012-10-26.
//  Copyright (c) 2012 Jason Brennan. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface JBSuggestionTableRowView : NSTableRowView

@end
